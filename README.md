# GitLab CI Configuration Documentation

## For the main branch

### Overview

This section outlines the current GitLab Continuous Integration (CI) configuration for the `main` branch. The setup is designed with flexibility in mind, accommodating future modifications and customizations as project requirements evolve.

### Usage

To work with this CI configuration for the `main` branch:

- **Trigger Pipeline via GitLab UI**: The pipeline can be manually triggered through the GitLab UI. Navigate to 'Pipelines' > 'Run pipeline', or follow [this link](https://gitlab.com/pkuttiya/ci-assignment/-/pipelines/new).
- **Define Input Variables**:
  - **Key**: Set the input variable key as `template`.
  - **Value**: Define the input variable value as `A`, `B`, or `C`, depending on the template you wish to execute.

## For the manual-pipeline branch

### Overview

This section details the GitLab CI configuration for the `manual-pipeline` branch. This configuration is focused on running tests for various applications in different environments and is automatically triggered by code changes pushed to this branch.

### Usage

The CI pipeline for the `manual-pipeline` branch automatically runs when code matching the paths specified in the CI configuration is pushed to the branch. Developers are responsible for updating the CI configuration file whenever new folders or paths are added to the repository, ensuring that corresponding tests are executed as part of the CI process.

## For the parent-child branch

### Overview

This section outlines the GitLab CI configuration for implementing parent-child pipelines. This configuration allows the triggering of specific child pipelines based on changes in distinct folders, enhancing modularity and efficiency.

### Usage

The parent-child CI pipeline operates under the following conditions:

- **Trigger Child Pipelines**: Jobs are defined to trigger child pipelines for different environments (dev, stage, prod).
- **Dynamic CI Paths**: The `CI_JOB_NAME` variable determines the specific child pipeline to trigger. For example, changes in the `dev/**/app.py` path will trigger the `dev` pipeline.
- **File Change Detection**: The pipeline triggers based on changes detected in specific file paths, ensuring that only relevant pipelines run for a given change.
